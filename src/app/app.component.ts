import {Component, ViewChild, OnInit, ElementRef, AfterViewInit} from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import { MatChipList} from '@angular/material/chips'
export interface ChipColor {
  name: string;
  color: ThemePalette;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit{
  availableColors: ChipColor[] = [
    {name: 'none', color: undefined},
    {name: 'Primary', color: 'primary'},
    {name: 'Accent', color: 'accent'},
    {name: 'Warn', color: 'warn'}
  ];

  height:number = 0;

  @ViewChild('color') colorList:MatChipList;

  @ViewChild('mydiv') mydiv: ElementRef;
  ngOnInit(): void {
    this.height = this.mydiv.nativeElement.offsetHeight;;
  }

  ngAfterViewInit(): void {
    console.log(this.colorList.chips.first);
  }

}